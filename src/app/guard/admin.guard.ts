import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  
  public user:any;

  constructor( 
    private _authService:AuthService,
    private _router:Router
  ){
  } 
  


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.user = this._authService.getUser();
      if(this.user.isAdmin){
        return true;
      }
      
      this._router.navigate(['/']);
      return false;
    
    
           
  }
  
}
