import {Prueba} from './Prueba.model';
export interface Jugador{
    id:number,
    Nombres:string,
    Apellidos:string,
    Cedula:string,
    FechaNacimiento:string,
    idCategoria:number,
    idUsuario:number,
    HorasJugadas:number,
    Foto:string,
    Lesion:string,
    entrenador:any,
    categoria:any,
    posicion:any,
    control:Array<Prueba>,
    createdAt:string,
    updatedAt:string
}