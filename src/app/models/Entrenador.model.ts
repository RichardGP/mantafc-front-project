export interface Entrenador{
    id:number;
    Username: string;
    Nombre: string;
    Apellido: string;
    Cedula:string;
    Password:string;
    Foto:any;
    estaActivo:boolean;
    idCategoria:number;
}