import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import jwtDecode from 'jwt-decode';

import { Router } from "@angular/router";
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url:String;

  constructor(private _http:HttpClient, private _router:Router) { 
    this.url = `${environment.apiURL}/auth`;
  }

  signIn(user):Observable<any>{
    return this._http.post(`${this.url}/signin`, user);
  }

  loggedIn(): Boolean{
    return !!localStorage.getItem('token');
  }

  setToken(token){
    localStorage.setItem("token", token);
  }

  getToken(){
    return localStorage.getItem('token');
  }

  getUser(){
    let decodedToken  = {user:{id:0, Apellido:"", Nombre:"", Username:"", estaActivo:false, isAdmin:false}};
    
    decodedToken = jwtDecode( this.getToken());
    return decodedToken.user;
  }

  loggout():void{
    localStorage.clear();
    this._router.navigate(['/']);
  }
}
