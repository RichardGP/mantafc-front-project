import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {

  private url = environment.apiURL+'/test';
  
  constructor(private _http:HttpClient) { }


  getTestById(id):Observable<any>{
    return this._http.get(`${this.url}/${id}`);
  }

  addTest(test):Observable<any>{
    return this._http.post(`${this.url}/create`, test);
  }

  updateTest(id, test){
    return this._http.patch(`${this.url}/update/${id}`,test);
  }
}
