import { ɵPLATFORM_WORKER_UI_ID } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JugadorService {
  private ruta = `${environment.apiURL}/player`;
  constructor(private _http:HttpClient) { }

  getPlayerByCategory(id:any, _idPeriodo:any):Observable<any>{
    let infoPeriodo = new FormData();
    infoPeriodo.append('idPeriodo', _idPeriodo);
    
    return this._http.get(`${this.ruta}/category/${id}/period/${_idPeriodo}`);
  }

  getPlayerById(id:any):Observable<any>{
    return this._http.get(`${this.ruta}/${id}`);
  }

  addPlayer(player):Observable<any>{
    let newPlayer = new FormData();
    newPlayer.append('Nombres', player.Nombres);
    newPlayer.append('Apellidos', player.Apellidos);
    newPlayer.append('Cedula', player.Cedula);
    newPlayer.append('FechaNacimiento', player.FechaNacimiento);
    newPlayer.append('idCategoria', player.idCategoria);
    newPlayer.append('idUsuario', player.idUsuario);
    newPlayer.append('Foto', player.Foto);
    newPlayer.append('Lesion', player.Lesion);
    newPlayer.append('idPosicion', player.idPosicion);
    newPlayer.append('idPeriodo', player.idPeriodo);
    
    return this._http.post(`${this.ruta}/create`, newPlayer);
  }

  updatePlayer(player):Observable<any>{
    let updatedPlayer = new FormData();
    updatedPlayer.append('Nombres', player.Nombres);
    updatedPlayer.append('Apellidos', player.Apellidos);
    updatedPlayer.append('Cedula', player.Cedula);
    updatedPlayer.append('FechaNacimiento', player.FechaNacimiento);
    updatedPlayer.append('idCategoria', player.idCategoria);
    updatedPlayer.append('idUsuario', player.idUsuario);
    updatedPlayer.append('Foto', player.Foto);
    updatedPlayer.append('Lesion', player.Lesion);
    updatedPlayer.append('idPosicion', player.idPosicion);
    updatedPlayer.append('idPeriodo', player.idPeriodo);

    return this._http.patch(`${this.ruta}/update/${player.id}`, updatedPlayer);
  }

}
