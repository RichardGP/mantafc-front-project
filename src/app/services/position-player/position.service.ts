import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  private  ruta = `${environment.apiURL}/position`;

  constructor(private _http:HttpClient) { }

  getPositions():Observable<any>{
    return this._http.get(this.ruta);
  }
}
