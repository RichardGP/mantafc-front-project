import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeriodoService {

  private ruta = `${environment.apiURL}/period`;

  constructor(private _http:HttpClient) { }

  getPeriodos():Observable<any>{
    return this._http.get(this.ruta);
  }
}
