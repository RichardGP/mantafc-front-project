import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  public url:String;
  constructor(private _http:HttpClient) {
    this.url = `${environment.apiURL}/categories`;
  }

  getCategoryById(id):Observable<any>{
    return this._http.get(`${this.url}/${id}`);
  }

  getCategories():Observable<any>{
    return this._http.get(`${this.url}`);
  }

  getTotalByCategory():Observable<any>{
    return this._http.get(`${this.url}/total/subs`);
  }

  getTotalCategoryById(id){
    return this._http.get(`${this.url}/total/subs/${id}`);
  }
}
