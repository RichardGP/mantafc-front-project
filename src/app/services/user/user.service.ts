import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  public ruta = `${environment.apiURL}/user`;
  
  constructor(private _http:HttpClient) { }

  getUser(id:any):Observable<any>{
    return this._http.get(`${this.ruta}/${id}`);
  }

  getUsers():Observable<any>{
    return this._http.get(`${this.ruta}`);
  }

  getUsersAdmin():Observable<any>{
    return this._http.get(`${this.ruta}/admin`);
  }

  getUsersTrainer():Observable<any>{
    return this._http.get(`${this.ruta}/trainer`);
  }

  getTrainerByCategoryId(id):Observable<any>{
    return this._http.get(`${this.ruta}/category/${id}`);
  }

  addUser(user):Observable<any>{
    const form = new FormData();
    if(user.esAdmin){
      form.append('Username',user.Username);
      form.append('Password', user.Password);
      form.append('Nombre',user.Nombre);
      form.append('Apellido',user.Apellido);
      form.append('Cedula',user.Cedula);
      form.append('Foto',user.Foto);
      form.append('esAdmin',user.esAdmin);
      form.append('estaActivo',user.estaActivo);
    }else{
      form.append('Username',user.Username);
      form.append('Password', user.Password);
      form.append('Nombre',user.Nombre);
      form.append('Apellido',user.Apellido);
      form.append('Cedula',user.Cedula);
      form.append('Foto',user.Foto);
      form.append('esAdmin',user.esAdmin);
      form.append('estaActivo',user.estaActivo);
      form.append('idCategoria',user.idCategoria);
    }
    return this._http.post(`${this.ruta}/create`,form);
  }

  updateUser(id, user):Observable<any>{
    const form = new FormData();
    console.log(user.Password);
    if(user.esAdmin){
      form.append('Username',user.Username);
      form.append('Password', user.Password);
      form.append('Nombre',user.Nombre);
      form.append('Apellido',user.Apellido);
      form.append('Cedula',user.Cedula);
      form.append('Foto',user.Foto);
      form.append('esAdmin',user.esAdmin);
      form.append('estaActivo',user.estaActivo);
    }else{
      form.append('Username',user.Username);
      form.append('Password', user.Password);
      form.append('Nombre',user.Nombre);
      form.append('Apellido',user.Apellido);
      form.append('Cedula',user.Cedula);
      form.append('Foto',user.Foto);
      form.append('esAdmin',user.esAdmin);
      form.append('estaActivo',user.estaActivo);
      form.append('idCategoria',user.idCategoria);
    }
    return this._http.patch(`${this.ruta}/update/${id}`, form);
  }

}
