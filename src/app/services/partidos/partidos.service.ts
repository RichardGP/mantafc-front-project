import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PartidosService {

  private ruta = `${environment.apiURL}/game-played`;
  
  constructor(private _http:HttpClient) { }

  getPartidosPorJugador(id:number):Observable<any>{
    return this._http.get(`${this.ruta}/player/${id}`);
  }

  addPartido(data):Observable<any>{
    return this._http.post(`${this.ruta}/create`, data);
  }

  getPartidoById(id):Observable<any>{
    return this._http.get(`${this.ruta}/${id}`);
  }

  updatePartido(id, game):Observable<any>{
    return this._http.patch(`${this.ruta}/update/${id}`,game);
  }
}

