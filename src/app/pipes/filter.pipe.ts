import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if(arg == null || arg.length < 2){
      return value;  
    }
    const filterUsers = [];
    for(let user of value){
      if(user.Nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        filterUsers.push(user);
      }
    }
    return filterUsers;
  }

}
