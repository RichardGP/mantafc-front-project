import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPlayer'
})
export class FilterPlayerPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if(arg == null || arg.length < 2){
      return value;  
    }
    const filterPlayers = [];
    for(let player of value){
      
      if(player.Nombres.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        filterPlayers.push(player);
      }
    }
    return filterPlayers;
  }

}
