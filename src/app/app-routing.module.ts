import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';

import {AuthComponent} from 'src/app/components/auth/auth.component';
import {PanelComponent} from 'src/app/components/panel/panel.component';
import {CategoryPlayerComponent} from 'src/app/components/category-player/category-player.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import {AdminComponent} from './components/admin/admin.component';
import {AdminAddComponent} from './components/admin-add/admin-add.component';
import {AdminUpdateComponent} from './components/admin-update/admin-update.component';
import {TrainerComponent} from './components/trainer/trainer.component';
import {TrainerAddComponent} from './components/trainer-add/trainer-add.component';
import {TrainerUpdateComponent} from './components/trainer-update/trainer-update.component';
import {PlayerComponent} from './components/player/player.component';
import {PlayerAddComponent} from './components/player-add/player-add.component';
import {PlayerUpdateComponent} from './components/player-update/player-update.component';

import {TestModalComponent} from './components/test-modal/test-modal.component';

import {AuthGuard} from "../app/guard/auth.guard";
import {AdminGuard} from './guard/admin.guard';



const routes: Routes = [
  {path:'', redirectTo:'/signin', pathMatch:'full'},
  {path:'signin', component:AuthComponent},
  {path:'panel', component:PanelComponent, canActivate:[AuthGuard]},
  {path:'category/:id',component:CategoryPlayerComponent, canActivate:[AuthGuard]},
  {path:'perfil/:id', component:PerfilComponent, canActivate:[AuthGuard]},
  {path:'admin', component:AdminComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'admin/add', component:AdminAddComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'admin/update/:id', component:AdminUpdateComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'trainer', component:TrainerComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'trainer/add', component:TrainerAddComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'trainer/update/:id', component:TrainerUpdateComponent, canActivate:[AuthGuard, AdminGuard]},
  {path:'player/add', component:PlayerAddComponent, canActivate:[AuthGuard]},
  {path:'player/:id', component:PlayerComponent, canActivate:[AuthGuard]},  
  {path:'player/update/:id', component:PlayerUpdateComponent, canActivate:[AuthGuard]},
  {path:'modal', component:TestModalComponent},
  {path:'**', component:PanelComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 