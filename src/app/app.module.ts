import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { PanelComponent } from './components/panel/panel.component';
import { TokenInterceptorService } from "./services/token-interceptor/token-interceptor.service";
import { CategoryPlayerComponent } from './components/category-player/category-player.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminAddComponent } from './components/admin-add/admin-add.component';
import { AdminUpdateComponent } from './components/admin-update/admin-update.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { TrainerAddComponent } from './components/trainer-add/trainer-add.component';
import { TrainerUpdateComponent } from './components/trainer-update/trainer-update.component';
import { PlayerComponent } from './components/player/player.component';
import { PlayerAddComponent } from './components/player-add/player-add.component';
import { PlayerUpdateComponent } from './components/player-update/player-update.component';
import { NavigationSupComponent } from './components/navigation-sup/navigation-sup.component';
import { GameModalComponent } from './components/game-modal/game-modal.component';
import { TestModalComponent } from './components/test-modal/test-modal.component';

import { FilterPlayerPipe } from './pipes/filter-player.pipe';
import { FilterPipe } from './pipes/filter.pipe';

import {AuthGuard} from './guard/auth.guard';
import {AdminGuard} from './guard/admin.guard';

import {SidebarModule} from 'ng-sidebar';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerModule} from 'ngx-spinner';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    PanelComponent,
    CategoryPlayerComponent,
    NavigationComponent,
    PerfilComponent,
    AdminComponent,
    AdminAddComponent,
    AdminUpdateComponent,
    TrainerComponent,
    FilterPipe,
    TrainerAddComponent,
    TrainerUpdateComponent,
    PlayerComponent,
    PlayerAddComponent,
    PlayerUpdateComponent,
    NavigationSupComponent,
    TestModalComponent,
    FilterPlayerPipe,
    GameModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    SidebarModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:TokenInterceptorService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
