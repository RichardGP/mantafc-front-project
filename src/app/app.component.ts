import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Front-MantaFC';
  token = localStorage.getItem('token');
  
  constructor(private _ngxSpinnerService:NgxSpinnerService){}
  
  ngOnInit(): void {
    this._ngxSpinnerService.show();

    setTimeout(()=>{
      this._ngxSpinnerService.hide();
    }, 2000);
  }

}
