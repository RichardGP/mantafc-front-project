import { Component, Input, OnInit } from '@angular/core';
import {Location} from '@angular/common';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { Test } from 'src/app/models/Test.model';

import { AuthService } from 'src/app/services/auth/auth.service';
import { PruebaService } from 'src/app/services/prueba/prueba.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-test-modal',
  templateUrl: './test-modal.component.html',
  styleUrls: ['./test-modal.component.css']
})
export class TestModalComponent implements OnInit {

  @Input() title:string = "Añadir Prueba";
  @Input() idJugador;
  @Input() idPrueba;

  public prueba:Test = {
    Peso:null,
    Estatura:null,
    Velocidad:null,
    CapacidadSalto:null,
    Resistencia:null,
    idJugador:null,
    idUsuario:null};

  constructor(public modal:NgbActiveModal, private _activateRoute:ActivatedRoute, 
    private _authService:AuthService, private _testService:PruebaService, private location:Location,
    private _router:Router, private _ngxSpinnerService:NgxSpinnerService) { }
  
  
  ngOnInit(): void {

    this.prueba.idJugador = this.idJugador;

    this.prueba.idUsuario = this._authService.getUser().id;
    
    if(this.idPrueba){
      this._ngxSpinnerService.show();
      this.cargarTest(this.idPrueba);
      this._ngxSpinnerService.hide();
    }
  }

  closeModal():void{
    this.modal.dismiss('Cancel');
    this.modal.dismiss('Cancel');
  }
  
  cargarTest(id):void{
    this._testService.getTestById(id).subscribe(
      res=>this.prueba=res,
      err=>{
      });
  }

  saveTest():void{
    if(this.idPrueba){
      this._testService.updateTest(this.idPrueba, this.prueba).subscribe(
        
        res=>{
          Swal.fire({
            title: 'Prueba actualizada',
            icon: 'success',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
            this.modal.close('Save Click');
            this._router.navigateByUrl('/panel', {skipLocationChange:true})
            .then(()=>{
              this._router.navigate([decodeURI(this.location.path())]);
            });  
          });
        },
        err=>{
          Swal.fire({
            title: 'Error al actualizar la prueba',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              
          });
        });

    }else{
      this._testService.addTest(this.prueba).subscribe(
        res=>{
          Swal.fire({
            title: 'Prueba guardada',
            icon: 'success',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
            this.modal.close('Save Click');
            this._router.navigateByUrl('/panel', {skipLocationChange:true})
            .then(()=>{
              this._router.navigate([decodeURI(this.location.path())]);
            });  
          });
        },
        err=>{
          Swal.fire({
            title: 'Error al guardar la prueba',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              
          });
        });
      }
    
      
  }

}
