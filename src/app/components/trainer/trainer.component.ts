import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';

import {Router} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  public users = [];
  public mostrar:boolean;
  public filterUsers:any = [];
  public actualPage=1;

  constructor(private _userService:UserService, private _router:Router, private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    
    this._ngxSpinnerService.show();

    this._userService.getUsersTrainer().subscribe(
      (res)=>{
        this.users = res;
        this._ngxSpinnerService.hide();
      },
      (err)=>{}
    );
  }

  cambiarNav(e){
    this.mostrar = e;
  }
}
