import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPlayerComponent } from './category-player.component';

describe('CategoryPlayerComponent', () => {
  let component: CategoryPlayerComponent;
  let fixture: ComponentFixture<CategoryPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
