import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, Params} from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {CategoriesService} from '../../services/categories/categories.service';
import {JugadorService} from '../../services/jugador/jugador.service';
import {PeriodoService} from '../../services/periodo/periodo.service';

import { TestModalComponent } from '../test-modal/test-modal.component';
import { GameModalComponent } from '../game-modal/game-modal.component';

import {Test} from '../../models/Test.model';
import {Game} from '../../models/Game.model';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-category-player',
  templateUrl: './category-player.component.html',
  styleUrls: ['./category-player.component.css']
})
export class CategoryPlayerComponent implements OnInit {

  public category:any = {};
  public jugadores:any = [];
  public periodos:any = [];
  public id:any;
  public idPeriodo:any;
  public categoryId:any;

  public mostrar:boolean;
  public filterPlayers:any = [];
  public serverURL:string;
  public actualPage = 1;

  constructor(private _categoriesService:CategoriesService,private _jugadorService:JugadorService,
    private _activatedRouter:ActivatedRoute, private _periodoService:PeriodoService,
    private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    this.serverURL = environment.serverURL;

    this._ngxSpinnerService.show();    
    this._periodoService.getPeriodos().subscribe(
      res =>{
        this.periodos = res;
        this._ngxSpinnerService.hide();
      },
      err=>{
        console.log(err);
        this._ngxSpinnerService.hide();
      }
    );
    

  }

  listarJugadores(){
    this._ngxSpinnerService.show();
    
    this._activatedRouter.params.subscribe((params:Params)=>{ 
      this.categoryId = params.id;
      this._categoriesService.getCategoryById(this.categoryId).subscribe(
        (res)=>{
          this.category = res;
          this._jugadorService.getPlayerByCategory(this.category.id, this.idPeriodo).subscribe(
            res =>{
              this.jugadores = res;
              this._ngxSpinnerService.hide();
            },            
            err=>{
              this._ngxSpinnerService.hide();
              console.log(err)
            }
          )
        },
        (err) =>{
          console.log(err);
          this._ngxSpinnerService.hide();
        }
      );
    }); 

  }

  cambiarNav(e){
    this.mostrar = e;
  }
}


