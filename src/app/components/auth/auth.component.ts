import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

import {ActivatedRoute, Router, Params} from '@angular/router';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public user = {
    Username:"",
    Password:""
  };

  public token:String;

  public validUser = true;
  public message = "";
  private name = "";
  constructor(private _authService:AuthService, private _router:Router, private _ngxSpinnerService:NgxSpinnerService) { 
    this.token = localStorage.getItem("token");
  }

  ngOnInit(): void {  

    this._ngxSpinnerService.show();

    setTimeout(()=>{
      this._ngxSpinnerService.hide();
    }, 500);
    
    if(this.token != null){
      this._router.navigate(['/panel']);
    }
    localStorage.setItem("showNav", JSON.stringify({mostrar:true}));
  }

  signIn():void{
    this._authService.signIn(this.user).subscribe(
      (res)=>{
        
        this._authService.setToken(res.token);
        
        this._router.navigate(['/panel']);      
      },    
      (err) =>{
        this.message = err.error.message || err.message;
        this.validUser = false;
        console.log(err)
        
      }
    );
  }



}
