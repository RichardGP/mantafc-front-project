import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { CategoriesService } from 'src/app/services/categories/categories.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-trainer-update',
  templateUrl: './trainer-update.component.html',
  styleUrls: ['./trainer-update.component.css']
})
export class TrainerUpdateComponent implements OnInit {

  public user = {
    id:0,
    Username: "",
    Nombre: "",
    Apellido: "",
    Cedula:"",
    Password:"",
    Foto:null,
    estaActivo:true,
    idCategoria:0
  };

  public Password:any;

  public mostrar:boolean;

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;
  public mensajeFoto:string;
  public listaCategorias = [];
  public serverURL:String;

  id = 1;
  
  constructor(private _userService:UserService, private _activeRouter:ActivatedRoute,
    private _categoriesService:CategoriesService,  private _router:Router, 
    private _ngxSpinnerService:NgxSpinnerService) {
      this.serverURL = environment.serverURL;
     }

  ngOnInit(): void {

    this._ngxSpinnerService.show();

    this._activeRouter.params.subscribe((params:Params)=>{
      
      this._userService.getUser(params.id).subscribe(
        (res)=>{
          res.Password = null;
          this.user = res;
          this._categoriesService.getCategories().subscribe(
            res =>{
              this.listaCategorias = res;
            },
            err =>{}
          );
          this._ngxSpinnerService.hide();
        },
        (err)=>{
          console.log(err);
        }
      );

      
    });

  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        this.mensajeFoto = element.target.files[0].name;
        //image preview
        const reader = new FileReader();
        
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
        this.user.Foto = this.foto;

    }
  }

  actualizarUsuario():void{
    this.user.Password = this.Password;
    this._userService.updateUser(this.user.id, this.user).subscribe(
      res=>{
        Swal.fire({
          title: 'Usuario actualizado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this._router.navigate(['/trainer']);
        });
      },
      err=>{
        Swal.fire({
          title: 'Error al actualizar el usuario',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      }
    );
  }

  cambiarNav(e){
    this.mostrar = e;
  }
}
