import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {JugadorService} from '../../services/jugador/jugador.service';
import {PartidosService} from '../../services/partidos/partidos.service';


import {Jugador} from '../../models/Jugador.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TestModalComponent } from '../test-modal/test-modal.component';
import { Test } from 'src/app/models/Test.model';
import { GameModalComponent } from '../game-modal/game-modal.component';
import { Game } from 'src/app/models/Game.model';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  public jugador:Jugador={
    id:0, Nombres:"", Apellidos:"", Cedula:"", FechaNacimiento:"",  idCategoria:0,  idUsuario:0,
    Foto:"",  Lesion:"", HorasJugadas:0, entrenador:{},  categoria:{}, posicion:{},  control:[{ id:0, Peso:0, Estatura:0,
    Velocidad:0, usuario:{id:0, Nombre:"", Apellido:""}, CreatedAt:"" }],
    createdAt:"",
    updatedAt:""
  };

  public partidos = [];
  public totalMinutos = 0;

  public mostrar:boolean;

  public fechas = []
  public velocidad:Array<number>= []
  public capacidadSalto:Array<number>= []
  public resistencia:Array<number>= []

  public chart = []
  public chart2 = []
  public chart3 = []

  public actualPageTest:number  = 1;
  public actualPageGame:number  = 1;
  public actualPageEstadoSalud:number = 1;

  public serverURL = environment.serverURL;
  
  public estadoSalud = [{fecha:'2020-12-13', presionArterial:100, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:100, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:100, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:100, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:101, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:102, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:103, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:104, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:105, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:106, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:107, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:108, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:109, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:109, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0},
  {fecha:'2020-12-13', presionArterial:189, temperatura:35, saturacionOxigeno: 98, etc1:0, etc2:0, etc3:0}];
s
  constructor(private _jugadorService:JugadorService, 
    private _router:Router, private _activateRouter:ActivatedRoute, private _partidosService:PartidosService,
    private _modalService:NgbModal, private _ngxSpinnerService:NgxSpinnerService) {
    
  }

  ngOnInit(): void {

    this._ngxSpinnerService.show();

    this._activateRouter.params.subscribe((params:Params)=>{
      this.llenarInformacionJugador(params.id);
      this.llenarPartidosJugados(params.id);
      this._ngxSpinnerService.hide();
    });
    
  }

  llenarPartidosJugados(idJugador){
    this._partidosService.getPartidosPorJugador(idJugador).subscribe(
      (res)=>{
      this.partidos = res;
      for (let partido of this.partidos) {
        this.totalMinutos+= partido.MinutosJugados;
      }
      
    },(err)=>{});
    
  }

  addTestModal():void{
    const ref = this._modalService.open(TestModalComponent);
    ref.componentInstance.Test = Test;
    ref.componentInstance.title = "Añadir prueba";
    ref.componentInstance.idJugador = this.jugador.id;
    
  }

  editTestModal( idTest):void{
    const ref = this._modalService.open(TestModalComponent);
    ref.componentInstance.Test = Test;
    ref.componentInstance.title = "Editar Prueba";
    ref.componentInstance.idJugador = this.jugador.id;
    ref.componentInstance.idPrueba = idTest;
    
  }

  addGameModal():void{
    let ref = this._modalService.open(GameModalComponent);
    ref.componentInstance.Game = Game;
    ref.componentInstance.idJugador = this.jugador.id;
    ref.componentInstance.title ="Añadir partido jugado";
  }

  editGameModal(idGame){
    let ref = this._modalService.open(GameModalComponent);
    ref.componentInstance.Game = Game;
    ref.componentInstance.idJugador = this.jugador.id;
    ref.componentInstance.idGame = idGame;
    ref.componentInstance.title ="Editar partido jugado";
  }



  llenarInformacionJugador(id:number){
    this._jugadorService.getPlayerById(id).subscribe(
      res => {
        
        this.velocidad = res['control'].map(value => parseFloat(value.Velocidad));
        this.fechas = res['control'].map(value => value.CreatedAt.substr(0,10));
        this.capacidadSalto = res['control'].map(value => parseFloat(value.CapacidadSalto));
        this.resistencia = res['control'].map(value => parseFloat(value.Resistencia));        

        this.jugador = res;

        this.chart = new Chart('canvasVelocidad', {
          type: 'line',
          data: {
            labels: this.fechas,
            datasets: [{
              label: 'Velocidad',
              data: this.velocidad,
              backgroundColor: [

                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
       
        this.chart2 = new Chart('canvasResistencia', {
          type: 'line',
          data: {
            labels: this.fechas,
            datasets: [{
              label: 'Resistencia',
              data: this.resistencia,
              backgroundColor: [
          
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      
      
        this.chart3 = new Chart('canvasSalto', {
          type: 'line',
          data: {
            labels: this.fechas,
            datasets: [{
              label: 'Capacidad de Salto',
              data: this.capacidadSalto,
              backgroundColor: [
          
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      }, 
      err=>{

      });
  }



  cambiarNav(e){
    this.mostrar = e;
  }

}
