import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navigation-sup',
  templateUrl: './navigation-sup.component.html',
  styleUrls: ['./navigation-sup.component.css']
})
export class NavigationSupComponent implements OnInit {
  @Output() toggleSidenav = new EventEmitter();
  
  mostrar:Boolean = true;
  
  constructor() { }

  ngOnInit(): void {
    
  }

  onToggleSidenav():void{
    if(this.mostrar){
      this.toggleSidenav.emit(this.mostrar);
      this.mostrar = false;
    }else{
      this.toggleSidenav.emit(this.mostrar);
      this.mostrar = true;
    }
  }

}
