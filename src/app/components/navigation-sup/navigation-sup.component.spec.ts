import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationSupComponent } from './navigation-sup.component';

describe('NavigationSupComponent', () => {
  let component: NavigationSupComponent;
  let fixture: ComponentFixture<NavigationSupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationSupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationSupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
