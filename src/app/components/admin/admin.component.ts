import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';

import {Router} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public users = [];
  public mostrar:boolean;
  public filterUsers:any = [];
  public actualPage=1;
  constructor(private _userService:UserService, private _ngxSpinnerService:NgxSpinnerService,private _router:Router) { }

  ngOnInit(): void {
    this._ngxSpinnerService.show();
    
    this._userService.getUsersAdmin().subscribe(
      (res)=>{
        this.users = res;
        this._ngxSpinnerService.hide();
      },
      (err)=>{}
    );
  }
  
  cambiarNav(e){
    this.mostrar = e;
  }
}
