import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import { environment } from 'src/environments/environment';

import {JugadorService} from '../../services/jugador/jugador.service';
import {CategoriesService} from '../../services/categories/categories.service';
import {PositionService} from '../../services/position-player/position.service';

import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-player-update',
  templateUrl: './player-update.component.html',
  styleUrls: ['./player-update.component.css']
})
export class PlayerUpdateComponent implements OnInit {
  public mostrar:boolean;
  public player = {
    Nombres:'',
    Apellidos:'',
    Cedula:'',
    FechaNacimiento:'', 
    idCategoria:0,
    idUsuario:0,
    Foto: null,
    Lesion:'', 
    idPosicion:0
  }

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;

  public listaPosiciones = [];
  public listaCategorias = [];
  
  public serverURL:String;

  constructor(private _jugadorService:JugadorService, private _activatedRouter:ActivatedRoute,
            private categoriaService:CategoriesService, private posicionService:PositionService,
            private location:Location, private _ngxSpinnerService:NgxSpinnerService) {
              this.serverURL = environment.serverURL;
             }

  ngOnInit(): void {

    this._ngxSpinnerService.show();

    this._activatedRouter.params.subscribe((params:Params)=>{
      this._jugadorService.getPlayerById(params.id).subscribe(
        (res)=>{
          this.player = res;
          this._ngxSpinnerService.hide();
        },      
        (err)=>{

        }
      );
    });

    this.posicionService.getPositions().subscribe(
      res=>{
        this.listaPosiciones = res;
      },
      err =>{

      }
    );
    this.categoriaService.getCategories().subscribe(
      res=>{
        this.listaCategorias = res;
      },
      err=>{

      }
    );
    
  }

  updatePlayer(){
    
    this._jugadorService.updatePlayer(this.player).subscribe(
      res=>{
        Swal.fire({
          title: 'Jugador actualizado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this.location.back();  
        });
      },
      err=>{
        Swal.fire({
          title: 'Error al actualizar el jugador',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      });
  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        
        //image preview
        const reader = new FileReader();
        
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
        this.player.Foto = this.foto;

    }
  }

  cambiarNav(e){
    this.mostrar = e;
  }

}
