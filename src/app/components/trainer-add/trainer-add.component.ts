import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

import { CategoriesService } from 'src/app/services/categories/categories.service';
import {Categoria} from '../../models/Categoria.model';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-trainer-add',
  templateUrl: './trainer-add.component.html',
  styleUrls: ['./trainer-add.component.css']
})
export class TrainerAddComponent implements OnInit {
  public userAdd = {
    Username: "",
    Password: "",
    Nombre: "",
    Apellido: "",
    Cedula: "",
    Foto:null,
    esAdmin: false,
    estaActivo: true,
    idCategoria:0
  };
  
  public categories:Array<Categoria>=[];

  
  public mostrar:boolean;

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;
  

  constructor(private _userService:UserService, 
    private _categoriesService:CategoriesService, private _router:Router,
    private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    this.categories = [{id:0,NombreCategoria:""}];
    
    this._ngxSpinnerService.show();    

    this._categoriesService.getCategories().subscribe(
      res =>{
        this.categories = res;
        this._ngxSpinnerService.hide();
      },
      err =>{}
    );
  }
 
  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
    
        this.userAdd.Foto = this.foto;

    }
  }

  guardarUsuario():void{
    
    this._userService.addUser(this.userAdd).subscribe(
      res=>{
        Swal.fire({
          title: 'Usuario Guardado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this._router.navigate(['/trainer']);
        });
      },
      err=>{
        Swal.fire({
          title: 'Error al guardar el usuario',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      }
    );
  }

  cambiarNav(e){
    this.mostrar = e;
  }

}
