import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';
import {CategoriesService} from '../../services/categories/categories.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { UserService } from 'src/app/services/user/user.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  
  public user:any;
  private id = 0;
  public isAdmin:any;
  public mostrar:boolean;
  public totalByCategories:any = [];
  
  constructor(private _router:Router, 
    private _authService:AuthService, 
    private _categoryService:CategoriesService, 
    private _userService:UserService,
    private _ngxSpinnerService:NgxSpinnerService ) { 
    
    }

  ngOnInit(): void {    
    
    this.id = this._authService.getUser().id;    
    
    this._ngxSpinnerService.show();

    this._userService.getUser(this.id).subscribe(
      (res)=>{
        this.user = res;
        this.isAdmin = this.user.esAdmin; 
        if(this.isAdmin){
          this.listOfCategoriesForAdmin();
        }else{
          
          this.listOfCategoriesById(this.user.idCategoria);
        }  
        this._ngxSpinnerService.hide();
      },      
      (err)=>{
        console.log(err);
        this._ngxSpinnerService.hide();
      });
      
  }

  listOfCategoriesById(id:any){ 
    this._categoryService.getTotalCategoryById(id).subscribe(
      (res)=>{
        this.totalByCategories.push(res);
      },
      (err)=>{}
      );
    
  }

  listOfCategoriesForAdmin(){    
    this._categoryService.getTotalByCategory().subscribe(
      res =>{
        this.totalByCategories = res.total;
      },
      err=>{

      }
    );
  }


  loggout():void {
    Swal.fire({
        title: '¿Estás seguro?',
        text: "La sesión actual sera cerrada",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#03A9F4',
        cancelButtonColor: '#F44336',
        confirmButtonText: 'Si, Salir!',
        cancelButtonText: 'Cancelar!'
      }).then((result)=> {
        if (result.value) {
          this._authService.loggout();
        }
      });
      
    }

    cambiarNav(e){
      this.mostrar = e;
    }
 
}
