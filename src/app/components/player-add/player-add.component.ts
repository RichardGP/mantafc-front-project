import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';

import {JugadorService} from '../../services/jugador/jugador.service';
import {CategoriesService} from '../../services/categories/categories.service';
import {PositionService} from '../../services/position-player/position.service';
import {AuthService} from '../../services/auth/auth.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user/user.service';
import { PeriodoService } from 'src/app/services/periodo/periodo.service';


@Component({
  selector: 'app-player-add',
  templateUrl: './player-add.component.html',
  styleUrls: ['./player-add.component.css']
})
export class PlayerAddComponent implements OnInit {

  public mostrar:boolean;
  public player = {
    Nombres:'',
    Apellidos:'',
    Cedula:'',
    FechaNacimiento:'', 
    idCategoria:0,
    idUsuario:0,
    Foto: null,
    Lesion:'', 
    idPosicion:0,
    idPeriodo:0
  }

  public trainer = {
    id:0,
    Nombre:'',
    Apellido:''
  }

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;

  public listaPosiciones = [];
  public listaCategorias = [];
  public listaPeriodos = [];

  constructor(private location:Location, private jugadorService:JugadorService,
              private categoriaService:CategoriesService, private posicionService:PositionService, 
              private periodoService:PeriodoService, private userService:UserService,
              private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    
    this._ngxSpinnerService.show();

    setTimeout(()=>{
      this._ngxSpinnerService.hide();
    }, 1000);
    
    this.llenarPosiciones();
    this.llenarCategorias();
    this.llenarPeriodos();
    
  }

  llenarPosiciones(){
    this.posicionService.getPositions().subscribe(
      res=>{
        this.listaPosiciones = res;
      },
      err =>{

      }
    );
  }
  
  llenarCategorias(){
    this.categoriaService.getCategories().subscribe(
      res=>{
        this.listaCategorias = res;
      },
      err=>{

      }
    );
  }

  llenarEntrenador(id){
    this.userService.getTrainerByCategoryId(id).subscribe(
      res=>{            
        this.trainer = res;        
        this.player.idUsuario = this.trainer.id;
      },
      err =>{
        
      }      
    );
  }

  llenarPeriodos(){
    this.periodoService.getPeriodos().subscribe(
      res=>{
        this.listaPeriodos = res;
      },
      err=>{}
    );
  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
    
        this.player.Foto = this.foto;

    }
  }

  addPlayer():void{      
    
    this.llenarEntrenador(this.player.idCategoria);
    this.jugadorService.addPlayer(this.player).subscribe(
      res=>{
        Swal.fire({
          title: 'Jugador guardado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this.location.back();  
        });        
      }, err=>{
        Swal.fire({
          title: 'Error al guardar el jugador',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      });

  }

  back():void{
    this.location.back();
  }

  cambiarNav(e){
    this.mostrar = e;
  }

}
