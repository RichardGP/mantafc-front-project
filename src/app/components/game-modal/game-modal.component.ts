import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

import { Game } from 'src/app/models/Game.model';

import { PartidosService } from 'src/app/services/partidos/partidos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-game-modal',
  templateUrl: './game-modal.component.html',
  styleUrls: ['./game-modal.component.css']
})
export class GameModalComponent implements OnInit {

  @Input() title:string = "Añadir Partido Jugado";
  @Input() idJugador;
  @Input() idGame;
  
  public game:Game={
    NombrePartido:null,
    MinutosJugados:null,
    FechaPartido:null,
    idJugador:0
  };


  constructor(public modal:NgbActiveModal, private _gameService:PartidosService, 
    private _ngxSpinnerService:NgxSpinnerService, private _router:Router, private location:Location) { }

  ngOnInit(): void {
    this.game.idJugador = this.idJugador;

    if(this.idGame){
      this._ngxSpinnerService.show();

      this._gameService.getPartidoById(this.idGame).subscribe(
        res=>{
          this.game=res
          this._ngxSpinnerService.hide();
        },
        err=>{
          Swal.fire({
            title: 'Error al guardar el juego',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              this.modal.dismiss('Cancel');
          });          
        }
      );
    }
  
  }

  closeModal():void{
    this.modal.dismiss('Cancel');
    this.modal.dismiss('Cancel');
  }
  
  saveGames():void{
    
    if(this.idGame){
      this._gameService.updatePartido(this.idGame, this.game).subscribe(
        res=>{
          Swal.fire({
            title: 'Juego Actualizado',
            icon: 'success',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
            this.modal.close('Save Click');          
            this._router.navigateByUrl('/panel', {skipLocationChange:true})
            .then(()=>{
              this._router.navigate([decodeURI(this.location.path())]);
            });
          });
        },
        err=>{
          Swal.fire({
            title: 'Error al actualizar el juego',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              
          });
        }
      );
    }else{
      this._gameService.addPartido(this.game).subscribe(
        res=>{
          Swal.fire({
            title: 'Juego Guardado',
            icon: 'success',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
            this.modal.close('Save Click');   
            this._router.navigateByUrl('/panel', {skipLocationChange:true})
            .then(()=>{
              this._router.navigate([decodeURI(this.location.path())]);
            });       
          });
        },
        err=>{
          Swal.fire({
            title: 'Error al guardar el juego',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              
          });
        }
      );
    }
  }


}
