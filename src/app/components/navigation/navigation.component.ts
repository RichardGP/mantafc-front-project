import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {CategoriesService} from '../../services/categories/categories.service';
import {UserService} from '../../services/user/user.service';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  @Input('mostrarNav') mostrarNav = true;
  
  public serverURL = environment.serverURL;
  public categories:any = [];
  public id:any;
  public user:any = {
    Username: "",
    Password: "",
    Nombre: "",
    Apellido: "",
    Cedula: "",
    Foto:null,
    esAdmin:true,
    categoria:{}
  };

  public mostrarGestionar;
  public mostrarJugadores;

  constructor(private _authService:AuthService, private _userService:UserService ,
    private _categoriesService:CategoriesService) { 
    }

  ngOnInit(): void {
  
    this.mostrarGestionar = false;
    this.mostrarJugadores = false;

    this.id = this._authService.getUser().id;
    this._userService.getUser(this.id).subscribe(
      (res)=>{
        this.user = res; 
        if(this.user.esAdmin){
          this.listOfCategoriesForAdmin();
        }else{
          this.listOfCategoriesById(this.user);
        }  
      },
      (err)=>{
        console.log(err);
      }
    ); 
    
  }

  listOfCategoriesById(user:any){ 
    this.categories.push(user.categoria);
  }

  listOfCategoriesForAdmin(){
    
    this._categoriesService.getCategories().subscribe(
      (res)=>{
        this.categories = res;
      },
      (err)=>{console.log(err)}
    );
  }

  loggout():void {
    Swal.fire({
      title: '¿Estás seguro?',
      text: "La sesión actual sera cerrada",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#03A9F4',
      cancelButtonColor: '#F44336',
      confirmButtonText: '<i class="zmdi zmdi-power"></i> SI, Salir!',
		  cancelButtonText: '<i class="zmdi zmdi-close-circle"></i> NO, Cancelar!'
    }).then((result)=> {
      if (result.value) {
        this._authService.loggout();
      }
    });
      
  }

  cambiarGestionar():void{
    if(this.mostrarGestionar == true){
      this.mostrarGestionar = false;
    }else{
      this.mostrarGestionar = true;
    }
  }

  cambiarJugadores():void{
    if(this.mostrarJugadores == true){
      this.mostrarJugadores = false;
    }else{
      this.mostrarJugadores = true;
    }
  }

  cambiarMostrarNav():void{
    this.mostrarNav = true;
    
  }
}
