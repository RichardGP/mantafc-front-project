import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';

import {ActivatedRoute, Params, Router} from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public user:any = {};

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;
  public savedPassword:any;
  public mostrar:boolean;
  public serverURL:string;

  constructor(private _userService:UserService, private _activeRouter:ActivatedRoute,
    private _router:Router,
    private _ngxSpinnerService:NgxSpinnerService) { 
      this.serverURL = environment.serverURL;
    }

  ngOnInit(): void {

    this._ngxSpinnerService.show();
    
    this._activeRouter.params.subscribe((params:Params)=>{
      
      this._userService.getUser(params.id).subscribe(
        (res)=>{
          this.savedPassword = res.Password;
          res.Password = null;
          this.user = res;
          this._ngxSpinnerService.hide();
        },
        (err)=>{
          this._ngxSpinnerService.hide();
          Swal.fire({
            title: 'Error al cargar la información',
            icon: 'error',
            confirmButtonColor: '#03A9F4',
            confirmButtonText: 'Ok!',
          }).then((result)=> {
              
          });
        }
      );
    });
  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
        this.user.Foto = this.foto;

    }
  }


  actualizarUsuario():void{
    if(!this.user.Password){
      this.user.Password = null;
    }
    console.log(this.user)
    this._userService.updateUser(this.user.id, this.user).subscribe(
      (res)=>{
        this._router.navigate(['/panel']);
      },
      (err)=>{
        console.log('No se ha podido actualizar el usuario');
      }
      );
  }

  cambiarNav(e){
    this.mostrar = e;
  }

}
