import { Component, OnInit } from '@angular/core';

import {UserService} from '../../services/user/user.service';

import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.css']
})
export class AdminAddComponent implements OnInit {

  public userAdd = {
    Username: "",
    Password: "",
    Nombre: "",
    Apellido: "",
    Cedula: "",
    Foto:null,
    esAdmin: true,
    estaActivo: true,
  };

  public Password2:String="";

  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;

  public mostrar:boolean;

  constructor(private _userService:UserService, private _router:Router, private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    this._ngxSpinnerService.show();

    setTimeout(()=>{
      this._ngxSpinnerService.hide();
    }, 2000);
  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
        this.userAdd.Foto = this.foto;

    }
  }

  guardarUsuario():void{
    this._userService.addUser(this.userAdd).subscribe(
      res=>{
        Swal.fire({
          title: 'Usuario Guardado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this._router.navigate(['/admin']);
        });
      },
      err=>{
        Swal.fire({
          title: 'Error al guardar el usuario',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      }
    );
  }
  
  cambiarNav(e){
    this.mostrar = e;
  }
}
