import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';

import {ActivatedRoute, Params, Router} from '@angular/router';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-admin-update',
  templateUrl: './admin-update.component.html',
  styleUrls: ['./admin-update.component.css']
})
export class AdminUpdateComponent implements OnInit {
  
  public user = {
    id:0,
    Username: "",
    Nombre: "",
    Apellido: "",
    Cedula:"",
    Password:"",
    Foto:null,
    estaActivo:true
  };

  public serverURL = environment.serverURL;
  public mostrar:boolean;
  
  public foto:File;
  public fotoSeleccionada:string | ArrayBuffer;
  public Password:string;
  public Password2:string;

  constructor(private _userService:UserService, private _activeRouter:ActivatedRoute,
    private _router:Router, private _ngxSpinnerService:NgxSpinnerService) { }

  ngOnInit(): void {
    this._ngxSpinnerService.show();

    this._activeRouter.params.subscribe((params:Params)=>{
      
      this._userService.getUser(params.id).subscribe(
        (res)=>{
          res.Password = null;
          this.user = res;
          this._ngxSpinnerService.hide();
        },
        (err)=>{
          
        }
      );
    });

  }

  fileChange(element) {
    if(element.target.files && element.target.files[0]){
        this.foto = <File>element.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = e  => this.fotoSeleccionada = reader.result;
        reader.readAsDataURL(this.foto);
        this.user.Foto = this.foto;

    }
  }

  actualizarUsuario():void{
    this.user.Password = this.Password;
    this._userService.updateUser(this.user.id, this.user).subscribe(
      res=>{
        Swal.fire({
          title: 'Usuario actualizado',
          icon: 'success',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
          this._router.navigate(['/admin']);
        });
      },
      err=>{
        Swal.fire({
          title: 'Error al actualizar el usuario',
          icon: 'error',
          confirmButtonColor: '#03A9F4',
          confirmButtonText: 'Ok!',
        }).then((result)=> {
            
        });
      }
    );
  }

  cambiarNav(e){
    this.mostrar = e;
  }
}
